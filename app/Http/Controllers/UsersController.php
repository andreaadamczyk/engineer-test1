<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Generator;
use \StdClass;

class UsersController extends Controller
{
	public function index(Request $request, Generator $faker) 
	{	
		$pmServer = 'http://10.100.64.5:8082';
		$endpoint = '/api/1.0/workflow/users';
		$accessToken = '0cceb182f05fbf4a7da5037d8f89c26adc10d7b8';

		$ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $pmServer . $endpoint);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken));
	    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	    $ret = curl_exec($ch);
	    $oRet = new StdClass;
	    $oRet->data = json_decode($ret);
	    curl_close($ch);
	    return json_encode($oRet);
	}

	public function approve() 
	{
		$pmServer = 'http://10.100.64.5:8082';
		$endpoint = '/api/1.0/workflow/plugin-test1/pass';
		$accessToken = '0cceb182f05fbf4a7da5037d8f89c26adc10d7b8';

		$ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $pmServer . $endpoint);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken));
	    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	    $ret = curl_exec($ch);
	    $oRet = new StdClass;
	    $oRet->data = json_decode($ret);
	    curl_close($ch);
	    return json_encode($oRet);

	}




}
