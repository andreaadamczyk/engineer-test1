
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');
window.$  = require( 'jquery' );
window.dt = require( 'datatables.net' );

const app = new Vue({
    el: '#app',
    mounted: function () {
        //Hello message
    	console.log("///////////////////////////");
    	console.log("Hello ProcessMaker");
    	console.log("///////////////////////////");

	    $('#example').DataTable( {
	    	"dom": 'rtip',
	        "ajax": {
                //url: '/api/users'
                url: 'http://10.100.64.5:8082/api/1.0/workflow/users',
                type: 'GET',
                async: true, // true
                dataSrc: '',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + "0cceb182f05fbf4a7da5037d8f89c26adc10d7b8");
                },
                contentType: 'application/json',
            },
            "columns": [
                {data:"usr_username"},
                {data:"usr_email"},
                {data:"usr_create_date"}
            ],
            "pageLength": 3
	    } );
    }
});
